FROM ubuntu:16.04 as builder

RUN apt-get update
RUN mkdir /zabbix
WORKDIR /zabbix
ADD https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+xenial_all.deb ./
RUN chmod +x zabbix-release_5.0-1+xenial_all.deb && dpkg -i zabbix-release_5.0-1+xenial_all.deb
RUN apt-get update  
RUN apt-get install -y zabbix-server-mysql 
ENV systemctl stop apache2 && systemctl disable apache2
RUN apt-get install -y nginx php-fpm 
RUN apt-get install -y zabbix-agent
ARG APP_ENV=production
ENV APP_ENV="${APP_ENV}"